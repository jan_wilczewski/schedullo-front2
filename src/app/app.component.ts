import {Component, Input} from '@angular/core';
import {CategoryModel} from './categories/common/category.model';
import {TaskService} from './task/common/task.service';
import {CategoryService} from './categories/common/category.service';
import {TaskModel} from './task/common/task.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {


}
