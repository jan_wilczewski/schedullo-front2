import {Component, Input, OnInit} from '@angular/core';
import {TaskModel} from './common/task.model';
import {TaskService} from './common/task.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {

  @Input()
  public task: TaskModel;

  constructor(private taskService: TaskService) { }

  onDelete() {
     this.taskService.deleteTask(this.task.id)
      .subscribe(response=> {
        console.log(response);
      })
  }

  ngOnInit() {
  }

}
