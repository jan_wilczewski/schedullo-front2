import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {TaskModel} from './task.model';

@Injectable()
export class TaskService {

  private readonly URL = 'http://localhost:8080/tasks';

  constructor(private httpClient: HttpClient){}

  public getAll(): Observable<Array<TaskModel>> {
    return this.httpClient.get<Array<TaskModel>>(this.URL + '/get');
  }

  public getTaskById(id: number): Observable<TaskModel> {
    return this.httpClient.get<TaskModel>(`this.URL/getid/${id}`);
  }

  public addTask(task: TaskModel): Observable<TaskModel> {
    return this.httpClient.post<TaskModel>(`${this.URL}/insert`, task);
  }

  public deleteTask(id: number): Observable<TaskModel> {
    return this.httpClient.delete<TaskModel>(`${this.URL}/delete/${id}`);
  }

  public updateCategory(categoryId: number, newCategoryId: number, task: TaskModel): Observable <TaskModel> {
    return this.httpClient.put<TaskModel>(`${this.URL}/udpate/${categoryId}//${newCategoryId}`, task);
  }

  public editTask(task: TaskModel): Observable<TaskModel> {
    return this.httpClient.put<TaskModel>(`this.URL/edit`, task);
  }


}
