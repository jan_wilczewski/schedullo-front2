import {CategoryModel} from '../../categories/common/category.model';

export class TaskModel {

  public id?: number;
  public title: string;
  public text: string;
  public taskStatus: TaskStatus;
  public category: CategoryModel;


}




export enum TaskStatus {
  TODO = 'TODO',
  SUSPENDED = 'SUSPENDED',
  IN_PROGRESS = 'IN_PROGRESS',
  SENT_FOR_APPROVAL = 'SENT_FOR_APPROVAL',
  REVISION = 'REVISION',
  ACCEPTED = 'ACCEPTED'
}
