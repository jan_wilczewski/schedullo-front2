import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {TaskModel} from '../../common/task.model';
import {TaskService} from '../../common/task.service';
import {CategoryModel} from '../../../categories/common/category.model';

@Component({
  selector: 'app-new-task-form',
  templateUrl: './new-task-form.component.html',
  styleUrls: ['./new-task-form.component.scss']
})
export class NewTaskFormComponent implements OnInit {

  status = ['', 'TODO', 'SUSPENDED', 'IN_PROGRESS', 'SENT_FOR_APPROVAL', 'REVISION', 'ACCEPTED'];

  newTaskForm: FormGroup;

  addedTask = new TaskModel();

  constructor(private formBuilder: FormBuilder, private taskService: TaskService) {
    this.createForm();
  }

  createForm() {
    this.newTaskForm = this.formBuilder.group({
      title: '',
      text: '',
      status: ''
    });
  }

  ngOnInit() {
  }

  onSubmit() {
    const val = this.newTaskForm.value;
    console.log(val);

    this.addedTask.title = this.newTaskForm.value.title;
    this.addedTask.text = this.newTaskForm.value.text;
    this.addedTask.taskStatus = this.newTaskForm.value.status;

    this.addedTask.category = new CategoryModel;
    this.setCategoryId(this.addedTask.taskStatus);

    console.log(this.addedTask);

    this.taskService.addTask(this.addedTask)
      .subscribe(
        (response) => console.log(response)
      );

    this.newTaskForm.reset();
  }

  private setCategoryId(status: string) {
    switch (status) {
      case "TODO": {
        this.addedTask.category.id = 1;
        break;
      }
      case "SUSPENDED": {
        this.addedTask.category.id = 2;
        break;
      }
      case "IN_PROGRESS": {
        this.addedTask.category.id = 3;
        break;
      }
      case "SENT_FOR_APPROVAL": {
        this.addedTask.category.id = 4;
        break;
      }
      case "REVISION": {
        this.addedTask.category.id = 5;
        break;
      }
      case "ACCEPTED": {
        this.addedTask.category.id = 6;
        break;
      }
    }
  }
}
