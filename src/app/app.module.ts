import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {HeaderComponent} from './header/header.component';
import {TaskComponent} from './task/task.component';
import {CategoriesComponent} from './categories/categories.component';
import {CategoryService} from './categories/common/category.service';
import {TaskService} from './task/common/task.service';
import {TaskAddComponent} from './task/task-add/task-add.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NewTaskFormComponent} from './task/task-add/new-task-form/new-task-form.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TaskComponent,
    CategoriesComponent,
    TaskAddComponent,
    NewTaskFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule.forRoot(),
    BrowserAnimationsModule,
  ],
  providers: [CategoryService, TaskService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
