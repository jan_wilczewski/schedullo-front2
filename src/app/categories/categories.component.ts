import {Component, Input, OnInit} from '@angular/core';
import {CategoryService} from './common/category.service';
import {CategoryModel} from './common/category.model';
import {TaskModel} from '../task/common/task.model';
import {TaskService} from '../task/common/task.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  public categories: CategoryModel[];

  @Input()
  public task: TaskModel;
  public category: CategoryModel;

  constructor(private categoryService: CategoryService, private taskService: TaskService) {
  }

  ngOnInit() {
    this.getCategories();
  }

  private getCategories() {
    this.categoryService.getAll().subscribe(categoriesLoadedFromBack => this.categories = categoriesLoadedFromBack);
  }


}
