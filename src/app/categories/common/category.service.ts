import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {CategoryModel} from './category.model';

@Injectable()
export class CategoryService {

  private readonly URL = 'http://localhost:8080/category';

  constructor(private hpptClient: HttpClient) {}

  public getAll(): Observable<Array<CategoryModel>> {
    return this.hpptClient.get<Array<CategoryModel>>(this.URL + '/get');
  }

}
