import {Task} from 'protractor/built/taskScheduler';

export class CategoryModel {

  public id?: number;
  private _name: string;
  private _tasks: Task[];




  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get tasks(): Task[] {
    return this._tasks;
  }

  set tasks(value: Task[]) {
    this._tasks = value;
  }
}
